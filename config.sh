#!/bin/bash

# Gtk3WindowsPackaging - A script and docs to help people package
# their own Rust program for Windows operating systems.
# 
# Copyright © 2022 Massimo Gismondi
# 
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>. 

# Title of the program
PROGRAM_NAME="Template"

# This will be used as the folder in the start menu
# which contains the link to the executable
COMPANY_NAME="ProjectGTK"

# The name of the built installer file. Add the .exe extension
INSTALLER_NAME="my_installer.exe"

# Path to the folder which contains the built binary.
# Insert a trailing backslash \ if it's not empty.
# A trailing backslash must be escaped as \\
BUILT_BINARY_FOLDER="C:\path\to\folder\\"

# The name of the binary in that folder. Add an extension if needed
# This is asked separately so the Uninstaller will be able to remove it
BUILT_BINARY_NAME="gtk-test.exe"

# Path to the icon folder. Add trailing backslash if the value is not empty
ICON_FOLDER=""
# The .ico file. It should be located in the ICON_FOLDER
ICON_NAME="icon.ico"
