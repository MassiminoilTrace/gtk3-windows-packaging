#!/bin/bash

# Gtk3WindowsPackaging - A script and docs to help people package
# their own Rust program for Windows operating systems.
# 
# Copyright © 2022 Massimo Gismondi
# 
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>. 



# Importing configuration
source "./config.sh"


cat > "installer.nsi"<< EOF
; The name of the installer, il titolo
Name "${PROGRAM_NAME}"

Unicode true

; The file to write
OutFile "${INSTALLER_NAME}"

; The default installation directory
InstallDir \$PROGRAMFILES64\\${COMPANY_NAME}

; this is needed for proper start menu item manipulation (for all users) in vista
RequestExecutionLevel admin

; This compressor gives us the best results
SetCompressor /SOLID lzma

; The text to prompt the user to enter a directory
DirText "This will install ${PROGRAM_NAME} on your computer. Choose a directory"

;--------------------------------

; The stuff to install
Section "" ;No components page, name is not important

; Set output path to the installation directory.

SetOutPath \$INSTDIR\bin
File "${BUILT_BINARY_FOLDER}${BUILT_BINARY_NAME}"

File bin\libatk-1.0-0.dll		; atk
File bin\libatkmm-1.6-1.dll		; atk
File bin\libssp-0.dll			; needed by cairo
File bin\libcairo-2.dll			; cairo, needed by gtk
File bin\libcairo-gobject-2.dll	; cairo. Doesn't seem to be required, but since we're distributing cairo...
File bin\libcairo-script-interpreter-2.dll  ; cairo. Doesn't seem to be required, but since we're distributing cairo...
File bin\libcairomm-1.0-1.dll
File bin\libepoxy-0.dll
File bin\libexslt-0.dll
File bin\libffi-7.dll  			; libffi is required by glib2 
File bin\libfontconfig-1.dll	; fontconfig is needed for ft2 pango backend
File bin\libfreetype-6.dll		; freetype is needed for ft2 pango backend
File bin\libfribidi-0.dll  ; fribidi is needed for pango 
File bin\libgailutil-3-0.dll	; from gtk
File bin\libgdk_pixbuf-2.0-0.dll  ; from gtk
File bin\liblzma-5.dll  		; from gtk
; File bin\libcroco-0.6-3.dll		; from gtk
File bin\libgdk-3-0.dll  		; from gtk
File bin\libgdkmm-3.0-1.dll
File bin\libgio-2.0-0.dll  		; from glib
File bin\libglib-2.0-0.dll  	; glib
File bin\libglibmm-2.4-1.dll  	; glib
File bin\libgiomm-2.4-1.dll  	; glib
File bin\libsigc-2.0-0.dll
File bin\libglibmm_generate_extra_defs-2.4-1.dll  ; glib
File bin\libgmodule-2.0-0.dll	; from glib
File bin\libgobject-2.0-0.dll	; from glib
File bin\libgthread-2.0-0.dll	; from glib
File bin\libgtk-3-0.dll  ; gtk
File bin\libgtksourceview-3.0-1.dll
File bin\libgtksourceview-4-0.dll
File bin\libgtksourceviewmm-3.0-0.dll
File bin\libgtkmm-3.0-1.dll
File bin\libharfbuzz-0.dll 		; required by pango
File bin\libintl-8.dll  		; gettext, needed by all i18n libs
File bin\libiconv-2.dll			; required by fontconfig
File bin\libjson-glib-1.0-0.dll	; gettext, needed by all i18n libs
File bin\libpango-1.0-0.dll  	; pango, needed by gtk
File bin\libpangocairo-1.0-0.dll  ; pango, needed by gtk
File bin\libpangowin32-1.0-0.dll  ; pango, needed by gtk
File bin\libpangoft2-1.0-0.dll	; pango, needed by gtk
File bin\libpangomm-1.4-1.dll
File bin\libpixman-1-0.dll  	; libpixman, needed by cairo
File bin\libpng16-16.dll  		; required by gdk-pixbuf2
File bin\libjpeg-8.dll  		; required by gdk-pixbuf2
File bin\libjasper-4.dll  		; required by gdk-pixbuf2
; File bin\libxml++-2.6-2.dll  ; fontconfig needs this
File bin\libxml++-3.0-1.dll
File bin\libxml2-2.dll			; fontconfig needs this
File bin\libxslt-1.dll			; fontconfig needs this
File bin\libpcre-1.dll			; fontconfig needs this
File bin\libthai-0.dll			; fontconfig needs this
File bin\libdatrie-1.dll			; fontconfig needs this
File bin\zlib1.dll				; png and many others need this
File bin\libexpat-1.dll			; required by fontconfig
File bin\libbz2-1.dll			; required by fontconfig
File bin\libgraphite2.dll		; required by harfbuzz
File bin\librsvg-2-2.dll		; required by adwaita-icon-theme
File bin\libtiff-5.dll			; required by gdk-pixbuf2
File bin\libstdc++-6.dll		; standard MSYS2 library
File bin\libgcc_s_seh-1.dll		; standard MSYS2 library
File bin\libwinpthread-1.dll	; standard MSYS2 library
File bin\libsoup-2.4-1.dll      ; libsoup
File bin\libsoup-gnome-2.4-1.dll      ; libsoup
File bin\libsqlite3-0.dll       ; libsoup dependency
File bin\libpsl-5.dll       ; libsoup dependency
File bin\libbrotlidec.dll       ; libsoup dependency
File bin\libbrotlicommon.dll       ; libsoup dependency
File bin\libgnutls-30.dll       ; glib-networking dependency
File bin\libgmp-10.dll		; glib-networking dependency
File bin\libhogweed-6.dll       ; glib-networking dependency
File bin\libnettle-8.dll	; glib-networking dependency
File bin\libidn2-0.dll		; glib-networking dependency
File bin\libp11-kit-0.dll	; glib-networking dependency
File bin\libtasn1-6.dll		; glib-networking dependency
File bin\libunistring-2.dll	; glib-networking dependency
File bin\libproxy-1.dll	; glib-networking dependency
File bin\libpeas-1.0-0.dll	; libpeas
File bin\libpeas-gtk-1.0-0.dll	; libpeas
File bin\libgirepository-1.0-1.dll	; gobject-introspection

File bin\gdbus.exe
File bin\fc-cache.exe
File bin\fc-cat.exe
File bin\fc-list.exe
File bin\fc-match.exe
File bin\fc-pattern.exe
File bin\fc-query.exe
File bin\fc-scan.exe
File bin\fc-validate.exe
File bin\gdk-pixbuf-query-loaders.exe  ; from gdk_pixbuf
File bin\gspawn-win64-helper.exe
File bin\gspawn-win64-helper-console.exe
File bin\gtk-query-immodules-3.0.exe
; File bin\gtk-update-icon-cache.exe


SetOutPath "\$INSTDIR\etc"
SetOverwrite off
File /r etc\gtk-3.0 
SetOverwrite On
File /r etc\fonts

SetOutPath "\$INSTDIR\lib\gdk-pixbuf-2.0\2.10.0"
File lib\gdk-pixbuf-2.0\2.10.0\loaders.cache

SetOutPath "\$INSTDIR\lib\gdk-pixbuf-2.0\2.10.0\"
File /r lib\gdk-pixbuf-2.0\2.10.0\loaders

SetOutPath "\$INSTDIR\lib\gio\modules"
File lib\gio\modules\libgiognutls.dll
File lib\gio\modules\libgiognomeproxy.dll
File lib\gio\modules\libgiolibproxy.dll

SetOutPath "\$INSTDIR\lib\libpeas-1.0\loaders"
File lib\libpeas-1.0\loaders\libpython3loader.dll

SetOutPath "\$INSTDIR\lib"
File /r lib\girepository-1.0

SetOutPath "\$INSTDIR\ssl\certs"
File ssl\certs\ca-bundle.crt
File ssl\certs\ca-bundle.trust.crt

SetOutPath "\$INSTDIR\share\locale"
File share\locale\locale.alias  ; from gettext

SetOutPath "\$INSTDIR\share\themes\Emacs"
File /r share\themes\Emacs\gtk-3.0
SetOutPath "\$INSTDIR\share\themes\Default"
File /r share\themes\Default\gtk-3.0

SetOutPath "\$INSTDIR\share\glib-2.0"
File /r share\glib-2.0\schemas

SetOutPath "\$INSTDIR\share"
File /r share\icons

SetOutPath "\$INSTDIR\share"
File /r share\gtksourceview-3.0

SetOutPath "\$INSTDIR\share"
File /r share\gtksourceview-4

; copio icone
SetOutPath "\$INSTDIR"
File "${ICON_FOLDER}${ICON_NAME}"
File "nsi_uninstall.ico"

SetOutPath \$INSTDIR
CreateDirectory "\$SMPROGRAMS\\${COMPANY_NAME}"
CreateShortCut "\$SMPROGRAMS\\${COMPANY_NAME}\\${PROGRAM_NAME}.lnk" "\$INSTDIR\bin\\${BUILT_BINARY_NAME}" "" "\$INSTDIR\\${ICON_NAME}"

CreateShortCut "\$SMPROGRAMS\\${COMPANY_NAME}\UNINSTALL ${PROGRAM_NAME}.lnk" "\$INSTDIR\uninstaller.exe" "" "\$INSTDIR\nsi_uninstall.ico"

WriteUninstaller \$INSTDIR\uninstaller.exe

SectionEnd ; end the section


Section "Uninstall"

Delete "\$INSTDIR\\${ICON_NAME}"
Delete "\$SMPROGRAMS\\${COMPANY_NAME}\\${PROGRAM_NAME}.lnk"
Delete "\$SMPROGRAMS\\${COMPANY_NAME}\UNINSTALL ${PROGRAM_NAME}.lnk"
RMDir "\$SMPROGRAMS\\${COMPANY_NAME}"
RMDir /r \$INSTDIR\bin
RMDir /r \$INSTDIR\lib
RMDir /r \$INSTDIR\etc
RMDir /r \$INSTDIR\share
RMDir /r \$INSTDIR\ssl

; Delete the uninstaller
Delete \$INSTDIR\uninstaller.exe

; Delete the directory
RMDir \$INSTDIR
SectionEnd
EOF
