# Windows packaging installer for GTK3

This project aims at building an installer to easily distribute a GTK program on Windows.

It bundles the entire GTK runtime into the package. This was built for a Rust program, but you can adapt it to whatever builds an executable file.

This is based on the work you can find at [this repo](https://github.com/tschoonj/GTK-for-Windows-Runtime-Environment-Installer).

## Note of the Author
I strongly suggest you to try to move your customers and users to an actually freedom-respecting operating system such as GNU/Linux.

Remember that publishing your software for Windows encourages users to stick that operating system.
Hence, you're actively contributing to hinder their access to the [four essential freedoms](https://www.gnu.org/philosophy/free-software-even-more-important.html) in software, you're making them more tracked and less free. As a software developer, your choices directly impact the people who use your product.

If you're unable to make them use a _free as in freedom_ operating system, here are your instructions for the windows installer.

# Prerequisites
These instructions have been tested on Windows 10 on x86_64 hardware.

## NSIS
You need to install [NSIS](https://nsis.sourceforge.io/Main_Page), which will build and bundle the installer.

~install NSIS pluging EnvVar~ (only needed for the original repo)

## Msys2
Install [msys2](https://www.msys2.org/) in `C:\msys64` folder

After this step, you should have a working msys2 environment.

We need to update msys2, so open it from the start menu and type:
```
pacman -Syu
```
It will probably close itself after some time. This happens because the update process has two steps.
Open it again and run again the previous command.
```
pacman -Syu
```


We need to install some basic libraries to install and compile GTK programs.
```
pacman -S --needed base-devel mingw-w64-x86_64-toolchain
pacman -S mingw-w64-x86_64-gtk3

pacman -S mingw-w64-x86_64-atkmm mingw-w64-x86_64-cairomm \
 mingw-w64-x86_64-libxslt mingw-w64-x86_64-gtkmm3 mingw-w64-x86_64-gtksourceview3 \
 mingw-w64-x86_64-gtksourceview4 mingw-w64-x86_64-gtksourceviewmm3 \
 mingw-w64-x86_64-libxml++ mingw-w64-x86_64-jasper \
 mingw-w64-x86_64-libsoup mingw-w64-x86_64-libpeas \
 mingw-w64-x86_64-libffi
```

## Rust compiler
In my case, I needed a rust compiler on windows. Download the executable from the [official website](https://www.rust-lang.org/tools/install).
When the process asks which toolchain to install, I suggest the GNU one.

Compiling an executable is out of the scope of this project.

# Using the tool
## Preparation
Clone this repository, then navigate to the folder using the msys2 bash terminal.


Run `./copy-msys2-files.sh` to collect the needed GTK libraries from the system folders to the current one.

Run again this command whenever you update the Msys2 packages with `pacman -Syu`.
Recompile also your program if needed.

## Building the executable
Properly configure `config.sh` file. Make sure to escape `\` and `$` if needed.
Most importantly, add `\` to the end of the folder paths. Remember that, in order to write a backslash at the end of a variable, you have to escape it in the `config.sh` script using a double backslash `\\`, because `\"` would escape the quote.
A correct example would be `FOLDER="C:\path\to\directory\\"`

Run `./create_nsis_script.sh`, which will build a `installer.nsi` NSIS script, properly configured based on the settings you provided in the previous step.
If the script has no execution permission, you can execute it anyway as `bash ./create_nsis_script.sh`.

Use NSIS to compile the `installer.nsi` to an executable you can redistribute.

Test the installer on another machine.

# Future improvements
Bundling and setting as default the Windows10 [theme](https://github.com/B00merang-Project/Windows-10) for GTK to [match](https://www.gtk.org/docs/installations/windows/#building-and-distributing-your-application) the correct "look and feel".

